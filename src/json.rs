use std::{
	fs::File,
	io::{BufReader, BufWriter},
};

use anyhow::{Context, Result};
use serde::Serialize;

use crate::context::Command;

// Required for enabling the usage of an iterator for serialization
struct IteratorAdapter<I>(I);

impl<I> Serialize for IteratorAdapter<I>
where
	I: Iterator + Clone,
	I::Item: Serialize,
{
	fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
	where
		S: serde::Serializer,
	{
		serializer.collect_seq(self.0.clone())
	}
}

/// Load commands from json file
///
/// * `path`: file path
pub fn load_commands(path: &std::path::Path) -> Result<Vec<Command>> {
	// Open file and reader
	let f = File::open(path).context("Failed to open file")?;
	let mut b = BufReader::new(&f);

	// Deserialize to json
	let cmds: Vec<Command> = serde_json::from_reader(&mut b)
		.with_context(|| format!("Failed to parse json in {}", path.display()))?;

	Ok(cmds)
}

/// Save commands iterator to json file
///
/// * `path`: file path
pub fn save_commands<I>(path: &std::path::Path, iter: I) -> Result<()>
where
	I: Clone + Iterator,
	I::Item: Serialize,
{
	// Open and truncate file writer
	let f = File::create(path).context("Failed to open file")?;
	let b = BufWriter::new(&f);

	// Write iterator to json file
	serde_json::to_writer(b, &IteratorAdapter(iter))?;

	Ok(())
}
