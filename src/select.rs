use anyhow::{anyhow, Result};
use arboard::Clipboard;
use std::{ffi::OsStr, fmt::Display};

use inquire::{Confirm, MultiSelect, Select, Text};

use crate::{
	context::{Command, Widget},
	prompt::print_seperator,
};

#[derive(Clone)]
struct ChoicePair<'a> {
	name: &'a str,
	cmd: &'a str,
}

impl<'a> Display for ChoicePair<'a> {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(f, "{}", self.name)
	}
}

pub fn select_cmd<'a>(data: &'a [Command]) -> Result<String> {
	// Create command name index for the selection
	let cmd_names: Vec<&str> = data.iter().map(|c| c.name.as_str()).collect();

	// Choose command
	let cmd = loop {
		// Prompt selection
		let selection = Select::new("Command:", cmd_names.clone()).raw_prompt()?;
		let selected_cmd = &data[selection.index];

		// Display command
		println!(
			"Description: {}\nCommand: {}",
			selected_cmd.desc, selected_cmd.cmd
		);
		selected_cmd.widgets.iter().for_each(|w| println!("{}", w));

		// Confirm command
		let confirm = Confirm::new("Want to proceed?").with_default(true).prompt()?;

		// Continue on yes
		if confirm {
			break selected_cmd;
		}
	};

	print_seperator();

	// Command to build from reverse
	let mut command = cmd.cmd.clone();

	for widget in cmd.widgets.iter().rev() {
		match widget {
			Widget::Input { pos, label } => {
				// Check if pos is within range
				if *pos > command.len() {
					return Err(anyhow!("Position {} is out of range", *pos));
				}

				// Prompt input
				let input = Text::new(&(label.to_owned() + ":")).prompt()?;
				// Insert into command
				command.insert_str(*pos, &input);
			},
			// Prompt a multiple choice and insert them all
			Widget::Choice { pos, label, choice } => {
				// Check if pos is within range
				if *pos > command.len() {
					return Err(anyhow!("Position {} is out of range", *pos));
				}
				// Prompt selection
				let selection = MultiSelect::new(
					&label,
					choice
						.iter()
						// Dont clone strings again
						.map(|c| ChoicePair {
							name: &c.label,
							cmd: &c.cmd,
						})
						.collect(),
				)
				.prompt()?;

				// join strings together seperated by space (ex. -dt -O3)
				let insert =
					selection.iter().map(|s| s.cmd).fold("".to_owned(), |a, c| a + &c + " ");

				// Insert into command
				command.insert_str(*pos, &insert);
			},
		}
	}

	Ok(command)
}

pub fn run_exe_select(data: &[Command]) -> Result<()> {
	// Run select dialog
	let command = select_cmd(data)?;

	println!("Running: {}", command);

	print_seperator();

	// Split command up ([0] is command, [1:] are parameters)
	let split: Vec<&OsStr> = command.split(' ').map(|s| OsStr::new(s)).collect();

	// Run Command and wait to finish
	let mut p = std::process::Command::new(split[0]).args(&split[1..]).spawn()?;
	let _ = p.wait()?;

	Ok(())
}

pub fn run_copy_select(data: &[Command]) -> Result<()> {
	// Run select dialog
	let command = select_cmd(data)?;

	print_seperator();

	// Print command
	println!("Command to copy paste: {}", command);

	// Copy to clipboard
	let mut cb = Clipboard::new().unwrap();
	cb.set_text(command).unwrap();

	// Prompt because clipboard deletes after application closes
	let _ = Confirm::new("Press enter to close").with_default(true).prompt()?;

	Ok(())
}
