use anyhow::{anyhow, Result};
use std::fmt::Display;

use serde::{Deserialize, Serialize};

// Holds data for a cmd choice
#[derive(Debug, Serialize, Deserialize)]
pub struct Choice {
	pub label: String,
	pub cmd: String,
}

// Was a widget before now just both input types
#[derive(Debug, Serialize, Deserialize)]
pub enum Widget {
	Input {
		pos: usize,
		label: String,
	},
	Choice {
		pos: usize,
		label: String,
		choice: Vec<Choice>,
	},
}

// Represent all the data a command should have
#[derive(Debug, Serialize, Deserialize)]
pub struct Command {
	pub name: String,
	pub desc: String,
	pub cmd: String,
	pub widgets: Vec<Widget>,
}

impl Display for Widget {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		match self {
			Widget::Input { label, .. } => {
				write!(f, "Input: {}", label)
			},
			Widget::Choice { label, choice, .. } => {
				// Choice always has at lease one element
				write!(f, "Choice: {}({}", label, choice[0].label)?;

                // Print rest
				for c in &choice[1..] {
					write!(f, ", {}", c.label)?;
				}

				write!(f, ")")
			},
		}
	}
}

pub fn validate_commands(mut cmds: Vec<Command>) -> Result<Vec<Command>> {
	// Sort for duplicate detection
	cmds.sort_unstable_by(|c1, c2| c1.name.cmp(&c2.name));

	// Remove duplicates
	cmds.dedup_by(|c1, c2| c1.name.eq(&c2.name));

	// Command is only valid if all attributes are filled out
	for c in &cmds {
		if c.cmd.is_empty()
			|| c.name.is_empty()
			|| c.desc.is_empty()
			|| c.widgets.iter().any(|w| match w {
				Widget::Input { pos, label } => *pos > c.cmd.len() || label.is_empty(),
				Widget::Choice { pos, label, choice } => {
					*pos > c.cmd.len()
						|| label.is_empty() || choice
						.iter()
						.any(|c| c.label.is_empty() || c.cmd.is_empty())
				},
			}) {
			return Err(anyhow!("The command {} has an error.", c.name));
		}
	}

	Ok(cmds)
}
