use std::path::PathBuf;

use anyhow::Result;
use clap::{Parser, ValueEnum};
use context::validate_commands;
use create::run_create;
use select::{run_copy_select, run_exe_select};

mod context;
mod create;
mod json;
mod prompt;
mod select;

#[derive(Copy, Clone, ValueEnum)]
enum Mode {
	/// Create a new command
	Create,
	/// Select a command and copy it. At best used with seperate terminal.
	Copy,
	/// Select a command and run it. It should be noted shell extensions like * or '' won't work.
	/// The parameters are directly inserted into the programm.
	Run,
}

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct Args {
	/// Path to the json file containing the commands to use
	#[arg(short, long)]
	path: PathBuf,

	/// Create a new command
	#[arg(short, long)]
	mode: Mode,
}

fn main() -> Result<()> {
	let args = Args::parse();

	// Load config file
	let data = json::load_commands(&args.path)?;

	// Validate
	let data = validate_commands(data)?;

	match args.mode {
		Mode::Create => run_create(&args.path, &data),
		Mode::Copy => run_copy_select(&data),
		Mode::Run => run_exe_select(&data),
	}
}
