use crate::{
	context::{Choice, Command, Widget},
	json::save_commands,
	prompt::print_seperator,
};
use anyhow::{anyhow, Result};
use inquire::{CustomType, Select, Text};
use std::{fmt::Display, path::Path, str::FromStr};

// Required to have a sexy error message for wrong input
#[derive(Clone)]
struct LabelCmdPair(String, String);

impl Display for LabelCmdPair {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(f, "Label: {}, Cmd: {}", self.0, self.1)
	}
}

impl FromStr for LabelCmdPair {
	type Err = ();

	fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
		let mut split = s.splitn(2, '/');
		if let (Some(label), Some(cmd)) = (split.next(), split.next()) {
			Ok(LabelCmdPair(label.to_owned(), cmd.to_owned()))
		} else {
			Err(())
		}
	}
}

pub fn run_create(path: &Path, cmds: &[Command]) -> Result<()> {
	// Promt command name
	let name = Text::new("Command name:").prompt()?;

	// Name not already used
	if cmds.iter().any(|c| c.name == name) {
		return Err(anyhow!("Name already used!"));
	};

	// Prompt command description
	let desc = Text::new("Description:").prompt()?;

	// Prompt command
	let mut cmd = Text::new("Command:")
		.with_help_message("The configuration can be done with ''")
		.prompt()?;

	// Store positions
	let mut positions = {
		// Get list of only ' characters & positions
		let mut iter = cmd.char_indices().filter(|(_, c)| *c == '\'').map(|(i, _)| i);

		// List of '' locations (point to first ')
		let mut pos: Vec<usize> = vec![];

		// Go through ' chars and if is '', save it
		while let Some(mut i) = iter.next() {
			while let Some(ii) = iter.next() {
				// If '' so ' is next to '
				if i + 1 == ii {
					pos.push(i); // Store beginning of ''
					break; // Skip next ' to avoid '''
				}

				i = ii;
			}
		}

		pos
	};

	// Remove '' from string & shorten positions to reflect that
	for (i, pos) in positions.iter_mut().enumerate() {
		*pos -= 2 * i;
		cmd.replace_range(*pos..*pos + 2, "");
	}

	// Inputs for command
	let mut widgets = Vec::with_capacity(positions.len());

	// Collect widgets based on insertions amount
	for (i, pos) in positions.iter().enumerate() {
		print_seperator();

		// Prompt for input type
		let t = Select::new(
			&format!("Select input type for {}", i),
			vec!["Choice", "Input"],
		)
		.with_help_message(&format!("{} to go", positions.len() - i))
		.raw_prompt()?;

		// Collect input for type
		widgets.push(match t.index {
			0 => {
				// Collect name
				let label = Text::new("Enter choice label:").prompt()?;

				let mut choice = vec![];

				// Label and cmd
				loop {
					match CustomType::<LabelCmdPair>::new("Enter choice:")
						.with_error_message("Must be seperated using a /")
						.with_help_message(
							"ESC to stop options. Enter the choice in this format <LABEL/CMD>",
						)
						.prompt()
					{
						Ok(LabelCmdPair(label, cmd)) => choice.push(Choice { label, cmd }),
						Err(inquire::InquireError::OperationCanceled) => break,
						Err(e) => return Err(e.into()),
					}
				}

				// At least one choice required
				if choice.is_empty() {
					return Err(anyhow!("At least one choice required."));
				}

				Widget::Choice {
					pos: *pos,
					label,
					choice,
				}
			},
			1 => {
				// Collect name
				let label = Text::new("Enter input label:").prompt()?;

				Widget::Input { pos: *pos, label }
			},

			// Only options 0, 1 are used
			_ => unreachable!(),
		});
	}

	// Construct Command
	let command = Command {
		name,
		desc,
		cmd,
		widgets,
	};

	// Write previous command and new command to file
	save_commands(
		Path::new(path),
		cmds.iter().chain(std::iter::once(&command)),
	)
}
